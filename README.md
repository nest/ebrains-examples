# EBRAINS Examples

Open directly in the EBRAINS JupyterLab:

* [NEST-Elephant example workflow](https://lab.ebrains.eu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.ebrains.eu%2Fnest%2Febrains-examples&urlpath=lab%2Ftree%2Febrains-examples%2Fsource%2Fnest-elephant-example%2FNEST-Elephant-Workflow.ipynb&branch=main)
* [PyNN-NEST example workflow](https://lab.ebrains.eu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.ebrains.eu%2Fnest%2Febrains-examples&urlpath=lab%2Ftree%2Febrains-examples%2Fsource%2Fpynn-nest-example%2FpyNN-NEST-Example.ipynb&branch=main)
