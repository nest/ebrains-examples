.. ebrains-examples documentation master file, created by
   sphinx-quickstart on Mon Feb 28 16:36:55 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ebrains-examples's documentation!
============================================

.. toctree::
   :maxdepth: 1
   :hidden:
   :glob:
   
   *




`NEST-Elephant workflow <nest-elephant-example/NEST-Elephant-Workflow.ipynb>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
